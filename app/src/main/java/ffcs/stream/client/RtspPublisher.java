package ffcs.stream.client;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

/**
 * RTSP推流
 */
public class RtspPublisher extends RtspClient {
    public static native int start(String inputFile, String outUrl);

    public static native void stop();


    private PublishThread publishThread;
    private File inputFile;


    public RtspPublisher(File inputFile2, String outUrl) {
        inputFile = inputFile2;
        publishThread = new PublishThread(inputFile.getAbsolutePath(), outUrl);
    }

    public File getInputFile() {
        return inputFile;
    }

    public void startPublish() {
        publishThread.start();
    }

    public void stopPublish() {
        RtspPublisher.stop();
        publishThread.interrupt();
        publishThread = null;
    }

    private static class PublishThread extends Thread {
        private String inputFile;
        private String outUrl;

        public PublishThread(String inputFile, String outUrl) {
            super();
            this.inputFile = inputFile;
            this.outUrl = outUrl;
        }

        @Override
        public void run() {
            RtspPublisher.start(inputFile, outUrl);
        }
    }

    private static final String DEFAULT_DIR = "ffcs-wisdom";

    private File getMP3Dri(Context context) throws IOException {
        File file = new File(getExternalFile(context).getAbsolutePath() + File.separator + "mp3");
//        File file = new File(getExternalFile(context).getAbsolutePath() + File.separator + "aac");
        if (!createDir(file)) {
            throw new IOException();
        }
        return file;
    }

    private File getExternalFile(Context context) throws IOException {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return context.getExternalFilesDir(null);
        } else {
            String path = context.getApplicationContext().getFilesDir().getAbsolutePath() + File.separator + DEFAULT_DIR;
            File file = new File(path);
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    throw new IOException();
                }
            }
            return file;
        }
    }

    private boolean createDir(File file) {
        return file.exists() || file.mkdirs();
    }
}
