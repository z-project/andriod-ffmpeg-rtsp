package ffcs.stream.client;

/**
 * Description: 结果回调
 * Created by zhangdong on 2021/6/21.
 */
interface ResultCallback {
    void onResult(int code, String msg);
}