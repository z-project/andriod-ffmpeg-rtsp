
#include <jni.h>
#include <string>
#include<android/log.h>
#include <exception>

//定义日志宏变量
#define logw(content)   __android_log_write(ANDROID_LOG_WARN,"eric",content)
#define loge(content)   __android_log_write(ANDROID_LOG_ERROR,"eric", content)
#define logd(content)   __android_log_write(ANDROID_LOG_DEBUG,"eric", content)

extern "C" {
#include "libavformat/avformat.h"
#include "libavformat/avio.h"
#include "libavutil/time.h"
#include "libavutil/avutil.h"
#include "libavutil/mathematics.h"
#include "libavutil/error.h"
}

#include <iostream>

using namespace std;

int avError(int errNum) {
    char buf[1024];
    //获取错误信息
    av_strerror(errNum, buf, sizeof(buf));
    loge(string().append("发生异常：").append(buf).c_str());
    return -1;
}

void my_av_input_dump_format(AVFormatContext *ic, int index,
                             const char *url, int is_output) {
    string pSrt = (is_output != 0) ? "output" : "input";

    loge(string().append("========音视频信息=======").c_str());

    loge(string().append("类型 ：").append(pSrt).c_str());
    loge(string().append("地址 ：").append(url).c_str());


    if (is_output) {
        loge(string().append("格式 ：").append(ic->oformat->name).c_str());
    } else {
        loge(string().append("格式 ：").append(ic->iformat->name).c_str());
    }

    loge(string().append("元数据").c_str());
    AVDictionaryEntry *tag = NULL;
    while ((tag = av_dict_get(ic->metadata, "", tag, AV_DICT_IGNORE_SUFFIX))) {
        loge(string().append(tag->key).append(":").append(tag->value).c_str());
    }

    loge(string().append("========================").c_str());
}


// 设置全局推送标识
int push_flag = 0;

extern "C"
JNIEXPORT jint JNICALL
Java_ffcs_stream_client_RtspPublisher_start(JNIEnv *env, jclass instance,
                                            jstring path_,
                                            jstring outPut) {

    //推流每一帧数据
    //int64_t pts  [ pts*(num/den)  第几秒显示]
    //int64_t dts  解码时间 [P帧(相对于上一帧的变化) I帧(关键帧，完整的数据) B帧(上一帧和下一帧的变化)]  有了B帧压缩率更高。
    //获取当前的时间戳  微妙
    long long start_time = av_gettime();

    if (push_flag == 1) {
        return 0;
    }



    push_flag = 1;

    loge("获取文件和推送地址 string -> char *");
    const char *inUrl = env->GetStringUTFChars(path_, JNI_FALSE);
    const char *outUrl = env->GetStringUTFChars(outPut, JNI_FALSE);
    loge(inUrl);
    loge(outUrl);

    loge("初始化");
    // 初始化组件将所有的编码器和解码器注册好
    //注册所有的编解码器
    avcodec_register_all();

    //注册所有的封装器
    av_register_all();

    //注册所有网络协议
    avformat_network_init();

    // 输入、输出的上下文
    AVFormatContext *ictx = NULL;
    AVFormatContext *octx = NULL;

    AVPacket pkt;
    int ret = 0;
    try {

        // 输入流处理部分 开始

        loge("封装输入的上下文 开始");
        ret = avformat_open_input(&ictx, inUrl, 0, 0);
        if (ret < 0) {
            avError(ret);
            throw ret;
        }
        loge("封装输入的上下文 结束");


        loge("获取音频视频的信息 开始");
        ret = avformat_find_stream_info(ictx, 0);
        if (ret != 0) {
            avError(ret);
            throw ret;
        }
        loge("获取音频视频的信息 结束");


        loge("打印输入音频视频的信息 开始");
        my_av_input_dump_format(ictx, -1, inUrl, 0);
        loge("打印输入音频视频的信息 结束");

        // 输入流处理部分 结束

        // 输出流处理部分 开始

        loge("封装输出的上下文 开始");
        // 输出流封装有问题
//        ret = avformat_alloc_output_context2(&octx, NULL, "flv", outUrl);//RTMP
//        ret = avformat_alloc_output_context2(&octx, NULL, "mpegts", outUrl);//UDP
        ret = avformat_alloc_output_context2(&octx, NULL, "rtsp", outUrl);//rtsp
        if (ret < 0) {
            avError(ret);
            throw ret;
        }
        loge("封装输出的上下文 结束");


        loge("封装输出的上下文-添加音视频流 开始");
        for (int i = 0; i < ictx->nb_streams; i++) {
            loge("封装输出的上下文-添加音视频流 执行中");

            // 获取输入视频流
            AVStream *in_stream = ictx->streams[i];
            // 输出上下文添加音视频流（初始化一个音视频流容器）
            AVStream *out_stream = avformat_new_stream(octx, in_stream->codec->codec);
            if (!out_stream) {
                loge("封装输出的上下文-添加音视频流 未能成功添加音视频流");
            }
            if (octx->oformat->flags & AVFMT_GLOBALHEADER) {
                out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
            }
            ret = avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
            if (ret < 0) {
                loge("封装输出的上下文-添加音视频流 复制编解码器上下文失败");
            }
            out_stream->codecpar->codec_tag = 0;
        }
        loge("封装输出的上下文-添加音视频流 结束");

        loge("打印输出音频视频的信息 开始");
        my_av_input_dump_format(octx, 0, outUrl, 1);
        loge("打印输出音频视频的信息 结束");



        // 推流处理部分 开始
        //https://stackoverflow.com/questions/49168678/ffmpeg-avio-open2-failed-to-open-output-rtsp-streams
        loge("打开IO 开始");
        //打开IO

//        ret = avio_open2(&octx->pb, outUrl, AVIO_FLAG_WRITE, &octx->interrupt_callback, NULL );
        ret = avio_open(&octx->pb, inUrl, AVIO_FLAG_WRITE);
        if (ret < 0) {
            avError(ret);
            throw ret;
        }
        loge("打开IO 结束");

        AVDictionary* options = NULL;
        av_dict_set(&options, "rtsp_transport", "tcp", 0);
        // av_dict_set(&options, "stimeout", "8000000", 0);
        av_dict_set(&options, "muxdelay", "0.1", 0);

        loge("写入头部信息 开始");
        ret = avformat_write_header(octx,&options);
        if (ret < 0) {
            avError(ret);
            throw ret;
        }
        loge("写入头部信息 结束");


        loge("找到视频流的位置 开始");
        int videoIndex = -1;
        for (int i = 0; i < ictx->nb_streams; i++) {
            if (ictx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
                videoIndex = i;
                break;
            }
        }
        loge("找到视频流的位置 结束");


        long long frame_index = 0;

        loge("推流 开始");
        while (push_flag) {
            //输入输出视频流
            AVStream *in_stream, *out_stream;


            logd("获取解码前数据 start >>>>>>>>>>>>>>>");
            //获取解码前数据
            ret = av_read_frame(ictx, &pkt);
            if (!push_flag || ret < 0) {
                continue;
            }
            logd("获取解码前数据 end >>>>>>>>>>>>>>>");
            /*
            PTS（Presentation Time Stamp）显示播放时间
            DTS（Decoding Time Stamp）解码时间
            */
            //没有显示时间（比如未解码的 H.264 ）
            if (pkt.pts == AV_NOPTS_VALUE) {

                logd("计算时间 start >>>>>>>>>>>>>>>");
                //AVRational time_base：时基。通过该值可以把PTS，DTS转化为真正的时间。
                AVRational time_base1 = ictx->streams[videoIndex]->time_base;

                //计算两帧之间的时间
                /*
                r_frame_rate 基流帧速率  （不是太懂）
                av_q2d 转化为double类型
                */
                int64_t calc_duration =
                        (double) AV_TIME_BASE / av_q2d(ictx->streams[videoIndex]->r_frame_rate);

                //配置参数
                pkt.pts = (double) (frame_index * calc_duration) /
                          (double) (av_q2d(time_base1) * AV_TIME_BASE);
                pkt.dts = pkt.pts;
                pkt.duration =
                        (double) calc_duration / (double) (av_q2d(time_base1) * AV_TIME_BASE);
                logd("计算时间 end >>>>>>>>>>>>>>>");
            }

            //延时
            if (pkt.stream_index == videoIndex) {
                logd("延时 start >>>>>>>>>>>>>>>");
                AVRational time_base = ictx->streams[videoIndex]->time_base;
                AVRational time_base_q = {1, AV_TIME_BASE};
                //计算视频播放时间
                int64_t pts_time = av_rescale_q(pkt.dts, time_base, time_base_q);
                //计算实际视频的播放时间
                int64_t now_time = av_gettime() - start_time;
                if (pts_time > now_time) {
                    //睡眠一段时间（目的是让当前视频记录的播放时间与实际时间同步）
                    av_usleep((unsigned int) (pts_time - now_time));
                }
                logd("延时 end >>>>>>>>>>>>>>>");
            }

            in_stream = ictx->streams[pkt.stream_index];
            out_stream = octx->streams[pkt.stream_index];

            //计算延时后，重新指定时间戳
            pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base,
                                       (AVRounding) (AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base,
                                       (AVRounding) (AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
            pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base,
                                        out_stream->time_base);
            pkt.pos = -1;

            if (pkt.stream_index == videoIndex) {
                logd("Send video frames to output >>>>>>>>>>>>>>>");
                printf("Send %lli video frames to output URL\n", frame_index);
                frame_index++;
            }


            logd("向输出上下文发送 start >>>>>>>>>>>>>>>");
            //向输出上下文发送（向地址推送）
            ret = av_interleaved_write_frame(octx, &pkt);
            if (ret < 0) {
                printf("发送数据包出错\n");
                break;
            }
            logd("向输出上下文发送 end >>>>>>>>>>>>>>>");
            //释放
            av_packet_unref(&pkt);
            logd("释放 end >>>>>>>>>>>>>>>");
        }

        loge("写入尾部信息 开始");
        ret = av_write_trailer(octx);
        if (ret < 0) {
            avError(ret);
            throw ret;
        }
        loge("写入尾部信息 结束");

        loge("推流 结束");
        ret = 0;
    } catch (int errNum) {
        push_flag = 0;
    }
    push_flag = 0;
    //关闭输出上下文，这个很关键。
    if (octx != NULL)
        avio_close(octx->pb);
    //释放输出封装上下文
    if (octx != NULL)
        avformat_free_context(octx);
    //关闭输入上下文
    if (ictx != NULL)
        avformat_close_input(&ictx);
    octx = NULL;
    ictx = NULL;
    return ret;
}


extern "C"
JNIEXPORT void JNICALL
Java_ffcs_stream_client_RtspPublisher_stop(JNIEnv *env, jclass clazz) {
    // 停止推流
    push_flag = 0;
}